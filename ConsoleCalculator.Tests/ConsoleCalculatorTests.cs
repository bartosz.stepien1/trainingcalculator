﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ConsoleCalculator.Tests
{
    public class ConsoleCalculatorTests 
    {
        [Theory]
        [InlineData(4,3,7)]
        [InlineData(21, 4.75, 25.75)]
        [InlineData(double.MaxValue, 5, double.MaxValue)]
        public static void Adding_SimpleValuesShouldCalculate(double a, double b, double expected)
        {
            //Arrange - we arrange our values

            //Act phase - we execute method
            double actual = CalcMethods.Adding(a, b);

            //Assert phase - we test if the outcome is correct
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(12.3, 3, 4.10)]
        [InlineData(8, 2, 4)]
        [InlineData(2134.3,0,0)]
        public void Divide_SimpleValuesShouldCalculate(double a, double b, double expected)
        {
            double actual = CalcMethods.Divide(a, b);

            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(5,2,10)]
        [InlineData(12.5,4,50)]
        public void Multiplication_SimpleValuesShouldCalculate(double a, double b, double expected)
        {
            double actual = CalcMethods.Multiplication(a, b);
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(12,8,4)]
        [InlineData(123.22,93,30.22)]
        public void Subtraction_SimpleValuesShouldCalculate(double a, double b, double expected)
        {
            double actual = CalcMethods.Subtraction(a, b);

            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(10,5,'/',2)]
        [InlineData(2,2,'+',4)]
        [InlineData(15.6,5.6,'-',10)]
        [InlineData(2.5,4,'*',10)]
        public void Calculate_SelectsCorrectCase(double a, double b, char opr, double expected)
        {
            double actual = CalcMethods.Calculate(a, b, opr);
            Assert.Equal(expected, actual);
        }
    }
}
