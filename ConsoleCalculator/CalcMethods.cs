﻿using System;
using System.Linq;
using System.Text;

namespace ConsoleCalculator
{
    public static class CalcMethods
    {
        private static readonly char[] _signs = { '+', '-', '*', '/' };
        private static readonly string[] _signsDesc = { "Dodawanie", "Odejmowanie", "Mnożenie", "Dzielenie" };
        static string DisplayHelp()
        {
            var sb = new StringBuilder();
            for (int i = 0; i < _signs.Length; i++)
            {
                sb.AppendLine($"{_signs[i]} -> {_signsDesc[i]}");
            }

            return sb.ToString();
        }


        static T TryUntilDie<T>(string message, Func<(bool, T)> func)
        {
            while (true)
            {
                Console.Clear();
                Console.Write(message);
                var funcResult = func();
                if (funcResult.Item1 == false)
                {
                    Console.Clear();
                    Console.WriteLine("Podano złą wartość, spróbuj ponownie!");
                    Console.ReadLine();
                }
                else
                {
                    return funcResult.Item2;
                }
            }
        }

        public static double GetNumber(string parameter)
        {
            return TryUntilDie<double>($"Podaj {parameter} liczbę: ", () =>
            {
                if (double.TryParse(Console.ReadLine().Replace('.', ','), out var result))
                {
                    Console.WriteLine(result);
                    return (true, result);
                }
                return (false, default);
            });
        }

        public static char GetValidationSign()
        {
            return TryUntilDie<char>(DisplayHelp(), () =>
            {
                char opr = Console.ReadKey().KeyChar;
                if (_signs.Contains(opr))
                {
                    Console.WriteLine(opr);
                    return (true, opr);
                }
                return (false, default);
            });
        }
        public static double Calculate(double a, double b, char opr)
        {
            switch (opr)
            {
                case '+':
                    return Adding(a, b);
                case '-':
                    return Subtraction(a, b);
                case '*':
                    return Multiplication(a, b);
                case '/':
                    return Divide(a, b);
                default:
                    throw new Exception("Tutaj nie powinno dojść");
            }
        }
        public static double Adding(double a, double b) => a + b;
        public static double Subtraction(double a, double b)
        {
            return a - b;
        }
        public static double Multiplication(double a, double b)
        {
            return Math.Round(a * b,2);
        }
        public static double Divide(double a, double b)
        {
            if (a != 0 && b != 0)
            {
                return Math.Round(a / b,2);
            }
            else
            {
                //cuz that's what i want !
                return 0;
            }
        }
    }
}
