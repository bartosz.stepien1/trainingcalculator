﻿using System;

namespace ConsoleCalculator
{
    public class Calculator
    {
        public static void Main(string[] args)
        {
            double a;
            double b;
            char opr;
            a = CalcMethods.GetNumber("pierwszą");
            Console.Clear();
            opr = CalcMethods.GetValidationSign();
            Console.Clear();
            b = CalcMethods.GetNumber("drugą");
            Console.Clear();
            Console.Write($"{a} {opr} {b} = ");
            Console.WriteLine(CalcMethods.Calculate(a, b, opr));
            Console.ReadKey();
        }
    }
}